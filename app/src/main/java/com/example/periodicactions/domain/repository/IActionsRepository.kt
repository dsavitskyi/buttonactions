package com.example.periodicactions.domain.repository

import com.example.periodicactions.domain.model.ActionTimeModel
import com.example.periodicactions.domain.model.ActionsModel
import com.example.periodicactions.domain.base.Result

interface IActionsRepository {

    suspend fun getActions(): Result<List<ActionsModel>>

    suspend fun updateSaveCoolDownTime(time: ActionTimeModel)

    suspend fun updateCoolDownTime(id: String, time: Long)

    suspend fun getCoolDownTimes(): List<ActionTimeModel>
}