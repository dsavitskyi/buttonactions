package com.example.periodicactions.domain.extensions

import java.time.DateTimeException
import java.time.DayOfWeek
import java.time.ZonedDateTime


fun ZonedDateTime.isCurrentDayOfWeek(dayPosition: Int): Boolean = try {
    this.dayOfWeek == DayOfWeek.of(dayPosition)
} catch (e: DateTimeException) {
    false
}

