package com.example.periodicactions.domain.managers

interface INotificationManager {

    suspend fun showNotification()
}