package com.example.periodicactions.domain.exceptions

class EmptyResponseException : RuntimeException()