package com.example.periodicactions.domain.model

enum class ActionType(val value: String) {
    ANIMATION("animation"),
    TOAST("toast"),
    CALL("call"),
    NOTIFICATION("notification"),
    IDLE("idle")
}