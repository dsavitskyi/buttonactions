package com.example.periodicactions.domain.model


data class ActionTimeModel(
    val actionId: String,
    val actionName: String,
    val coolDownTime: Long
)