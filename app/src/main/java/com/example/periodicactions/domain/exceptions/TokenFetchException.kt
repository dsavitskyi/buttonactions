package com.example.periodicactions.domain.exceptions

class TokenFetchException : RuntimeException()