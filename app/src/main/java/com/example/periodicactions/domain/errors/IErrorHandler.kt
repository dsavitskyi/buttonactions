package com.example.periodicactions.domain.errors

import com.example.periodicactions.domain.errors.Error

interface IErrorHandler {

    fun getError(tr: Throwable): Error
}