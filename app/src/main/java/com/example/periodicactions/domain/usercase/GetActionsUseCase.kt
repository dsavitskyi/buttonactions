package com.example.periodicactions.domain.usercase

import com.example.periodicactions.domain.extensions.isCurrentDayOfWeek
import com.example.periodicactions.domain.model.ActionTimeModel
import com.example.periodicactions.domain.model.ActionType
import com.example.periodicactions.domain.repository.IActionsRepository
import java.time.Duration
import java.time.Instant
import java.time.ZonedDateTime
import java.time.temporal.ChronoUnit
import java.util.UUID
import com.example.periodicactions.domain.base.Result
import com.example.periodicactions.domain.errors.Error
import com.example.periodicactions.domain.model.ActionResult
import javax.inject.Inject

class GetActionsUseCase @Inject constructor(
    private val actionsRepository: IActionsRepository
) : UseCase<UseCase.EmptyParams, ActionResult> {

    override suspend fun execute(params: UseCase.EmptyParams): ActionResult {
        when(val actions = actionsRepository.getActions()) {
            is Result.Failure -> {
                when(actions.error) {
                    is Error.NetworkConnectionError,
                    is Error.InternetConnectionError -> {
                        return ActionResult(
                            actionType = ActionType.IDLE,
                            error = true
                        )
                    }
                    else -> {
                        return ActionResult(
                            actionType = ActionType.IDLE,
                            error = true
                        )
                    }
                }
            }
            is Result.Success -> {
                val activeActions = actions.data.filter { it.enabled &&
                        it.validDays.any { ZonedDateTime.now().isCurrentDayOfWeek(it + 1) }
                }

                val coolDownTimes = actionsRepository.getCoolDownTimes()

                val filteredActions = if (coolDownTimes.isNotEmpty() && activeActions.isNotEmpty()) activeActions.filter { model ->
                    val days = calculateCoolDownDays(model.coolDown, model.validDays.size)
                    val currentActionTime = coolDownTimes.firstOrNull { it.actionName == model.type }?.coolDownTime
                    currentActionTime?.let {
                        isCoolDownPassed(it, days)
                    }?: true
                } else activeActions

                if (filteredActions.isNotEmpty()) {
                    val currentAction = filteredActions.sortedByDescending { it.priority }.first().apply {
                        actionsRepository.updateSaveCoolDownTime(ActionTimeModel(
                            actionId = UUID.randomUUID().toString(),
                            actionName = this.type,
                            coolDownTime = Instant.now().toEpochMilli()))
                    }
                    return ActionResult(
                        actionType = ActionType.values().firstOrNull { it.value == currentAction.type }?: ActionType.IDLE
                    )
                }
            }
        }
        return ActionResult(actionType = ActionType.IDLE)
    }

    private fun isCoolDownPassed(previousTimestamp: Long, days: Long): Boolean {
        val previousTime = Instant.ofEpochMilli(previousTimestamp)
        return Duration.between(previousTime, Instant.now()) > Duration.of(days, ChronoUnit.DAYS)
    }

    private fun calculateCoolDownDays(coolDownPeriod: Long, daysNumber: Int): Long {
        val duration = coolDownPeriod.times(60).times(60).times(24).times(daysNumber)
        return Duration.ofMillis(duration).toDays()
    }
}