package com.example.periodicactions.domain.model

data class ActionResult(
    val actionType: ActionType,
    val error: Boolean = false
)