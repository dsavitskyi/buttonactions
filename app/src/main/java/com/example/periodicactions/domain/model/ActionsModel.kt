package com.example.periodicactions.domain.model


data class ActionsModel(
    val coolDown: Long,
    val enabled: Boolean,
    val priority: Int,
    val type: String,
    val validDays: List<Int>
)