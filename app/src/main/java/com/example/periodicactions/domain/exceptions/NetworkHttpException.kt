package com.example.periodicactions.domain.exceptions

import java.io.IOException

data class NetworkHttpException(val code: Int, val errorMessage: String?) : IOException()