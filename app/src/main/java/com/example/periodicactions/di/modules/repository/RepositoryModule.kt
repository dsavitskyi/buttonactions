package com.example.periodicactions.di.modules.repository

import com.example.periodicactions.data.repository.ActionsRepository
import com.example.periodicactions.domain.repository.IActionsRepository
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
interface RepositoryModule {

    @Binds
    @Singleton
    fun bindActionsRepository(repo: ActionsRepository): IActionsRepository

}