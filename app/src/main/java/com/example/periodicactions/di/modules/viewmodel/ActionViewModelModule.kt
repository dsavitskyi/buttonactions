package com.example.periodicactions.di.modules.viewmodel

import androidx.lifecycle.ViewModel
import com.example.periodicactions.presentation.screens.ActionViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
interface ActionViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(ActionViewModel::class)
    fun bindActionViewModel(model: ActionViewModel): ViewModel
}