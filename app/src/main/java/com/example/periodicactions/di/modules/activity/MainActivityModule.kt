package com.example.periodicactions.di.modules.activity

import androidx.lifecycle.ViewModel
import com.example.periodicactions.di.modules.viewmodel.ActionViewModelModule
import com.example.periodicactions.di.modules.viewmodel.ViewModelKey
import com.example.periodicactions.di.scope.FragmentScope
import com.example.periodicactions.presentation.MainViewModel
import com.example.periodicactions.presentation.screens.ActionFragment
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
interface MainActivityModule {

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    fun bindMainViewModel(model: MainViewModel): ViewModel

    @FragmentScope
    @ContributesAndroidInjector(modules = [ActionViewModelModule::class])
    fun actionFragment(): ActionFragment
}