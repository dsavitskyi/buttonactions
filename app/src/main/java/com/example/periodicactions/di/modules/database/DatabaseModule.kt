package com.example.periodicactions.di.modules.database

import android.content.Context
import androidx.room.Room
import com.example.periodicactions.data.database.ActionsDatabase
import com.example.periodicactions.data.database.dao.ActionsDao
import com.example.periodicactions.di.annotations.ApplicationContext
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
object DatabaseModule {

    @Provides
    @Singleton
    fun provideDatabase(
        @ApplicationContext context: Context
    ): ActionsDatabase {
        return Room.databaseBuilder(context, ActionsDatabase::class.java, ActionsDatabase.NAME).build()
    }


    @Provides
    @Singleton
    fun provideActionsDao(database: ActionsDatabase): ActionsDao {
        return database.getActionsDao()
    }
}