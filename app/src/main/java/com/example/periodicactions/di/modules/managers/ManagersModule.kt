package com.example.periodicactions.di.modules.managers

import com.example.periodicactions.domain.managers.INotificationManager
import com.example.periodicactions.presentation.managers.NotificationManager
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
interface ManagersModule {

    @Binds
    @Singleton
    fun bindNotificationManager(manager: NotificationManager): INotificationManager

}