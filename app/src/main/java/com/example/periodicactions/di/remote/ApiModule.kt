package com.example.periodicactions.di.remote

import com.example.periodicactions.data.network.ApiProvider
import com.example.periodicactions.data.network.IApiProvider
import dagger.Binds
import dagger.Module
import dagger.Reusable

@Module
interface ApiModule {

    @Binds
    @Reusable
    fun bindApiProviderModule(helper: ApiProvider): IApiProvider
}