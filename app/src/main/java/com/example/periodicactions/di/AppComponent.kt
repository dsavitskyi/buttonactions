package com.example.periodicactions.di

import android.content.Context
import com.example.periodicactions.di.annotations.ApplicationContext
import com.example.periodicactions.di.modules.activity.ActivityBindingModule
import com.example.periodicactions.di.modules.activity.MainActivityModule
import com.example.periodicactions.di.modules.database.DatabaseModule
import com.example.periodicactions.di.modules.managers.ManagersModule
import com.example.periodicactions.di.modules.repository.RepositoryModule
import com.example.periodicactions.di.modules.viewmodel.ViewModelModule
import com.example.periodicactions.di.remote.ApiModule
import com.example.periodicactions.presentation.App
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        ActivityBindingModule::class,
        ViewModelModule::class,
        MainActivityModule::class,
        ApiModule::class,
        RepositoryModule::class,
        DatabaseModule::class,
        ManagersModule::class
    ]
)
interface AppComponent : AndroidInjector<App> {


    @Component.Factory
    interface Factory {

        fun create(@BindsInstance @ApplicationContext context: Context): AppComponent
    }
}