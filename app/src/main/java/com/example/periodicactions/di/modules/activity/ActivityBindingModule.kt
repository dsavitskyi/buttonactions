package com.example.periodicactions.di.modules.activity

import com.example.periodicactions.di.scope.ActivityScope
import com.example.periodicactions.presentation.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
interface ActivityBindingModule {

    @ActivityScope
    @ContributesAndroidInjector(modules = [MainActivityModule::class])
    fun mainActivity(): MainActivity
}