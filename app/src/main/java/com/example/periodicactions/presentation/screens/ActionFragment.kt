package com.example.periodicactions.presentation.screens

import android.content.Intent
import android.os.Bundle
import android.provider.ContactsContract
import android.view.View
import android.view.animation.LinearInterpolator
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import com.example.periodicactions.R
import com.example.periodicactions.databinding.FragmentActionBinding
import com.example.periodicactions.domain.extensions.collectWithLifecycle
import com.example.periodicactions.domain.extensions.toastSh
import com.example.periodicactions.domain.model.ActionType
import com.example.periodicactions.presentation.ViewBindingFragment
import com.example.periodicactions.presentation.utils.Constants.NOTIFICATION_ACTION_TYPE
import javax.inject.Inject

class ActionFragment: ViewBindingFragment<FragmentActionBinding>(FragmentActionBinding::inflate) {

    @Inject
    lateinit var factory: ViewModelProvider.Factory
    val viewModel: ActionViewModel by viewModels(factoryProducer = { factory })

    private val startContactsForResult = registerForActivityResult(
        ActivityResultContracts.StartActivityForResult()
    ) { result: ActivityResult -> }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeViewModel()
        setListeners(binding)
        getNotificationAction()
    }

    private fun setListeners(binding: FragmentActionBinding) {
        binding.btnAction.setOnClickListener {
            viewModel.getCurrentActions()
        }
    }

    private fun observeViewModel() {
        viewModel.actionTypeFlow.collectWithLifecycle(viewLifecycleOwner) {
            when(it) {
                is ActionViewModel.ActionTypeResult.Loading -> {
                    showProgressLoading(true)
                }
                is ActionViewModel.ActionTypeResult.Success -> {
                    showProgressLoading(false)
                    if (it.result.error) {
                        toastSh(R.string.label_error)
                    } else {
                        when(it.result.actionType) {
                            ActionType.ANIMATION -> {
                                animateButton()
                            }
                            ActionType.TOAST -> {
                                toastSh(R.string.label_action_toast)
                            }
                            ActionType.CALL-> {
                                openContacts()
                            }
                            ActionType.NOTIFICATION-> {
                                viewModel.showActionNotification()
                            }
                            ActionType.IDLE -> {
                                toastSh(R.string.label_no_action)
                            }
                        }
                    }
                }
                is ActionViewModel.ActionTypeResult.Idle -> {
                    showProgressLoading(false)
                }
            }
        }
    }

    private fun showProgressLoading(isVisible: Boolean) {
        binding.flActionProgress.isVisible = isVisible
    }

    private fun animateButton() {
        binding.btnAction.animate().rotationBy(360f).setInterpolator(LinearInterpolator()).start()
    }

    private fun openContacts() {
        val pickContact = Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI)
        startContactsForResult.launch(pickContact)
    }

    private fun getNotificationAction() {
        arguments?.getBoolean(NOTIFICATION_ACTION_TYPE).apply {
            if (this == true) openContacts()
        }
    }
}