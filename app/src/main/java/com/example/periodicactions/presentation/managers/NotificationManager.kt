package com.example.periodicactions.presentation.managers

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import android.os.Bundle
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.navigation.NavDeepLinkBuilder
import com.example.periodicactions.R
import com.example.periodicactions.di.annotations.ApplicationContext
import com.example.periodicactions.domain.managers.INotificationManager
import com.example.periodicactions.presentation.utils.Constants.NOTIFICATION_ACTION_TYPE
import javax.inject.Inject

class NotificationManager @Inject constructor(
    @ApplicationContext private val context: Context
) : INotificationManager {

    override suspend fun showNotification() {
        val manager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createNotificationChannel(manager)
        }
        createNotification(
            context = context,
            notificationManager = manager
        )
    }

    private fun createNotification(
        context: Context,
        notificationManager: NotificationManager
    ) {
        val pendingIntent = NavDeepLinkBuilder(context)
            .setArguments(Bundle().apply { putBoolean(NOTIFICATION_ACTION_TYPE, true) })
            .setGraph(R.navigation.main_navigation)
            .addDestination(R.id.actionFragment)
            .createPendingIntent()

        val notification = NotificationCompat.Builder(
            context,
            ACTION_NOTIFICATION_CHANEL_ID
        )
            .setStyle(NotificationCompat.BigTextStyle())
            .setSmallIcon(com.google.android.material.R.drawable.ic_clock_black_24dp)
            .setContentTitle(context.getString(R.string.app_name))
            .setContentText(context.getString(R.string.label_action_notification))
            .setContentIntent(pendingIntent)
            .setAutoCancel(true)
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            .build()

        notificationManager.notify(ACTION_NOTIFICATION_ID, notification)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel(notificationManager: NotificationManager) {
        val channel = NotificationChannel(
            ACTION_NOTIFICATION_CHANEL_ID,
            ACTION_NOTIFICATION_CHANEL_NAME,
            NotificationManager.IMPORTANCE_DEFAULT
        )
        notificationManager.createNotificationChannel(channel)
    }

    companion object {
        private const val ACTION_NOTIFICATION_ID = 1135
        private const val ACTION_NOTIFICATION_CHANEL_ID = "ACTION_NOTIFICATION_CHANEL_ID"
        private const val ACTION_NOTIFICATION_CHANEL_NAME = "Action event notification"
    }
}