package com.example.periodicactions.presentation.screens

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.periodicactions.domain.managers.INotificationManager
import com.example.periodicactions.domain.model.ActionResult
import com.example.periodicactions.domain.usercase.GetActionsUseCase
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

class ActionViewModel @Inject constructor(
    private val getActionsUseCase: GetActionsUseCase,
    private val notificationManager: INotificationManager
) : ViewModel() {

    private val _actionTypeFlow = MutableStateFlow<ActionTypeResult>(ActionTypeResult.Idle)
    val actionTypeFlow: StateFlow<ActionTypeResult> = _actionTypeFlow

    fun getCurrentActions() {
        viewModelScope.launch {
            _actionTypeFlow.update { ActionTypeResult.Loading }
            val actionType = getActionsUseCase.execute()
            _actionTypeFlow.update { ActionTypeResult.Success(actionType) }
        }
    }

    fun showActionNotification() {
        viewModelScope.launch {
            notificationManager.showNotification()
        }
    }

    sealed class ActionTypeResult {
        object Loading: ActionTypeResult()
        data class Success(val result: ActionResult): ActionTypeResult()
        object Idle: ActionTypeResult()
    }
}