package com.example.periodicactions.data.datasource.model

import com.google.gson.annotations.SerializedName

data class ActionsResponse(
    @SerializedName("cool_down") val coolDown: Long,
    @SerializedName("enabled") val enabled: Boolean,
    @SerializedName("priority") val priority: Int,
    @SerializedName("type") val type: String,
    @SerializedName("valid_days") val validDays: List<Int>
)