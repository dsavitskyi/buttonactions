package com.example.periodicactions.data.network

import com.example.periodicactions.domain.exceptions.TokenFetchException
import com.example.periodicactions.domain.extensions.logd
import okhttp3.Interceptor
import okhttp3.Response
import java.util.concurrent.ExecutionException
import java.util.concurrent.TimeoutException
import javax.inject.Inject

class RequestAuthInterceptor @Inject constructor() : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        logd(TAG) { "intercept: request = ${chain.request().url}" }
        val request = chain.request().newBuilder()
//        val currentUser = FirebaseAuth.getInstance().currentUser
//        if (currentUser != null) {
//            val token = getUserToken(currentUser)
//            request.addHeader(
//                AUTHORIZATION_HEADER,
//                "$AUTHORIZATION_HEADER_TOKEN_TYPE $token"
//            )
//        }

        return chain.proceed(request.build())
    }

//    @Throws(
//        TokenFetchException::class,
//        ExecutionException::class,
//        InterruptedException::class,
//        TimeoutException::class
//    )
//    private fun getUserToken(user: FirebaseUser): String {
//        logd(TAG) { "intercept: currentUser id = ${user.uid}" }
//        val tokenResult = user.getIdToken(false).get(AUTH_CONNECT_TIMEOUT_MILLIS)
//        logd(TAG) { "intercept: tokenResult = $tokenResult" }
//        val token = tokenResult.getOrThrow().token ?: throw  TokenFetchException()
//        logd(TAG) { "token = $token" }
//        return token
//    }


    companion object {
        private val TAG = RequestAuthInterceptor::class.java.name
        private const val AUTHORIZATION_HEADER = "Authorization"
        private const val AUTHORIZATION_HEADER_TOKEN_TYPE = "Bearer"
        private const val AUTH_CONNECT_TIMEOUT_MILLIS = 10_000L
    }
}

