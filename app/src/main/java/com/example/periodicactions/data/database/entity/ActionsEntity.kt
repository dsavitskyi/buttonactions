package com.example.periodicactions.data.database.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "actions")
data class ActionsEntity constructor(
    @PrimaryKey
    @ColumnInfo(name = "_id")
    val actionId: String,

    @ColumnInfo(name = "action_name")
    val actionName: String,

    @ColumnInfo(name = "cool_down_time")
    val coolDownTime: Long
)