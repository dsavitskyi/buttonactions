package com.example.periodicactions.data.datasource.remote

import com.example.periodicactions.data.datasource.model.ActionsResponse
import com.example.periodicactions.data.network.IApiProvider
import retrofit2.http.GET
import javax.inject.Inject

class ActionsRemoteDataSource @Inject constructor(apiProvider: IApiProvider) {

    private val actionsApi = apiProvider.getApi(ACTIONS_API_URL, ActionsApi::class.java, false)

    suspend fun getActions(): List<ActionsResponse> {
        return actionsApi.getActionsData()
    }

    interface ActionsApi {
        @GET("butto_to_action_config.json")
        suspend fun getActionsData(): List<ActionsResponse>
    }

    companion object {
        private const val ACTIONS_API_URL = "https://s3-us-west-2.amazonaws.com/androidexam/"
    }
}