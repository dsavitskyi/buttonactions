package com.example.periodicactions.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.periodicactions.data.database.dao.ActionsDao
import com.example.periodicactions.data.database.entity.ActionsEntity

const val DATABASE_VERSION = 1

@Database(
    version = DATABASE_VERSION,
    entities = [
        ActionsEntity::class
    ],
    exportSchema = true
)

abstract class ActionsDatabase : RoomDatabase() {

    companion object {
        internal const val NAME = "actions.db"
    }

    abstract fun getActionsDao(): ActionsDao
}