package com.example.periodicactions.data.mapper

import com.example.periodicactions.data.database.entity.ActionsEntity
import com.example.periodicactions.data.datasource.model.ActionsResponse
import com.example.periodicactions.domain.model.ActionTimeModel
import com.example.periodicactions.domain.model.ActionsModel


fun ActionsResponse.toDomain() = ActionsModel(
    coolDown = coolDown,
    enabled = enabled,
    priority = priority,
    type = type,
    validDays = validDays
)

fun ActionsEntity.toDomain() = ActionTimeModel(
    actionId = actionId,
    actionName = actionName,
    coolDownTime = coolDownTime
)

fun ActionTimeModel.toEntity() = ActionsEntity(
    actionId = actionId,
    actionName = actionName,
    coolDownTime = coolDownTime
)