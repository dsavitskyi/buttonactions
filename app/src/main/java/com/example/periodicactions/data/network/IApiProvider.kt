package com.example.periodicactions.data.network

interface IApiProvider {

    fun <T> getApi(baseUrl: String, service: Class<T>, requiresAuthentication: Boolean = true): T
}