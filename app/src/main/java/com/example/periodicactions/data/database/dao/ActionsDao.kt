package com.example.periodicactions.data.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.periodicactions.data.database.entity.ActionsEntity

@Dao
interface ActionsDao {

    @Query("SELECT * FROM actions")
    suspend fun getAllTimes(): List<ActionsEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(action: ActionsEntity)

    @Query("UPDATE actions SET cool_down_time=:time WHERE action_name = :name")
    suspend fun updateActionTimes(name: String, time: Long)

    @Query("SELECT * FROM actions WHERE action_name =:name")
    suspend fun getTimeByName(name: String): ActionsEntity?
}