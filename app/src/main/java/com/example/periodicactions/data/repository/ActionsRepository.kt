package com.example.periodicactions.data.repository

import com.example.periodicactions.data.database.dao.ActionsDao
import com.example.periodicactions.data.datasource.remote.ActionsRemoteDataSource
import com.example.periodicactions.data.mapper.toDomain
import com.example.periodicactions.data.mapper.toEntity
import com.example.periodicactions.domain.base.Result
import com.example.periodicactions.domain.extensions.executeSafeWithResult
import com.example.periodicactions.domain.model.ActionTimeModel
import com.example.periodicactions.domain.model.ActionsModel
import com.example.periodicactions.domain.repository.IActionsRepository
import javax.inject.Inject

class ActionsRepository @Inject constructor(
    private val actionsRemoteDataSource: ActionsRemoteDataSource,
    private val actionsDao: ActionsDao
): IActionsRepository {

    override suspend fun getActions(): Result<List<ActionsModel>> =
        executeSafeWithResult {
            Result.Success(actionsRemoteDataSource.getActions().map { it.toDomain() })
        }

    override suspend fun updateSaveCoolDownTime(time: ActionTimeModel) {
        actionsDao.getTimeByName(time.actionName)?.let {
            actionsDao.updateActionTimes(time.actionName, time.coolDownTime)
        }?: actionsDao.insert(time.toEntity())
    }

    override suspend fun updateCoolDownTime(id: String, time: Long) {
        actionsDao.updateActionTimes(id, time)
    }

    override suspend fun getCoolDownTimes(): List<ActionTimeModel> =
        actionsDao.getAllTimes().map { it.toDomain() }
}